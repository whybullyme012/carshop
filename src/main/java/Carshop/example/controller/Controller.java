package Carshop.example.controller;

import Carshop.example.Enums.*;
import Carshop.example.domain.*;
import Carshop.example.es.CarEntity;
import Carshop.example.es.CustomerEntity;
import Carshop.example.es.OrderEntity;
import Carshop.example.service.CarService;
import Carshop.example.service.CustomerService;
import Carshop.example.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    private CarService carService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;


    @GetMapping("/searchDataCar")
    public List<CarEntity> searchDataCar(SearchCar car){ return carService.searchDataCar(car);}

    @GetMapping("/getAllDatacar")
    public List<CarEntity> getAllDataCar(){return carService.getAlldataCar();}

    @PostMapping("/addNewCar")
    public void addCar(@RequestParam ChooseCar carName, @RequestParam ColorCar color) { carService.addCar(carName,color); }

    @PutMapping("/bookingCar")
    public Booking bookingCar(@RequestParam ChooseCar carName, @RequestParam ColorCar color, @RequestParam CarShowRoom showRoom, @RequestParam double cashPledge
            , @RequestParam ChoosePromotion promotion, @RequestBody Customer personalInformation, @RequestParam Gender gender){return carService.bookingCar(carName,color,showRoom,cashPledge,promotion,personalInformation,gender);}

    @GetMapping("/getAllDataCustomer")
    public List<CustomerEntity> getAllCustomer() { return customerService.getAllDataCustomer(); }

    @PostMapping("/addCustomer")
    public void addCustomer(@RequestBody Customer customer ,@RequestParam Gender gender) { customerService.addCustomer(customer,gender); }

    @GetMapping("/getIdOrderByid")
    public OrderEntity getIdOrderById(@RequestParam String id){return orderService.getIdorderById(id);}

    @GetMapping("/getAllDataOrder")
    public List<OrderEntity> getAllOrder(){return orderService.getAllOrder();}

    @PutMapping({"/cashPayment"})
    public ReceiptCash cashPayment(@RequestParam String idOrder, @RequestParam double cash) { return orderService.cashPayment(idOrder,cash); }

    @PutMapping({"/monthPayment"})
    public ReceiptMonth monthPayment(@RequestParam String idOrder, @RequestParam double cash) { return orderService.monthPayment(idOrder,cash); }

}