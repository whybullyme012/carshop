package Carshop.example.domain;

import Carshop.example.Enums.CarShowRoom;
import Carshop.example.Enums.ChooseCar;
import Carshop.example.Enums.ColorCar;

import java.time.LocalDate;

public class Booking {
    private String idOrder;
    private ChooseCar carName;
    private ColorCar colorCar;
    private double cashDown;
    private double allPayment;
    private double monthlyPayment;
    private CarShowRoom carShowRoom;
    private String firstName;
    private String lastName;
    private String tel;
    private String address;
    private String postalCode;
    private String email;
    private LocalDate bookingDate;

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public ChooseCar getCarName() {
        return carName;
    }

    public void setCarName(ChooseCar carName) {
        this.carName = carName;
    }

    public ColorCar getColorCar() {
        return colorCar;
    }

    public void setColorCar(ColorCar colorCar) {
        this.colorCar = colorCar;
    }

    public double getCashDown() {
        return cashDown;
    }

    public void setCashDown(double cashDown) {
        this.cashDown = cashDown;
    }

    public double getAllPayment() {
        return allPayment;
    }

    public void setAllPayment(double allPayment) {
        this.allPayment = allPayment;
    }

    public double getMonthlyPayment() {
        return monthlyPayment;
    }

    public void setMonthlyPayment(double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public CarShowRoom getCarShowRoom() {
        return carShowRoom;
    }

    public void setCarShowRoom(CarShowRoom carShowRoom) {
        this.carShowRoom = carShowRoom;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDate bookingDate) {
        this.bookingDate = bookingDate;
    }
}
