package Carshop.example.domain;

public class Customer {


        private String firstName;
        private String lastName;
        private int age;
        private String tel;
        private String driverLicense;
        private String address;
        private String postalCode;
        private String email;



        public String getFirstName() { return firstName; }
        public void setFirstName(String firstName) { this.firstName = firstName; }

        public String getLastName() { return lastName; }
        public void setLastName(String lastName) { this.lastName = lastName; }

        public int getAge() {
                return age;
        }
        public void setAge(int age) {
                this.age = age;
        }

        public String getTel() { return tel; }
        public void setTel(String tel) { this.tel = tel; }

        public String getAddress() { return address; }
        public void setAddress(String address) { this.address = address; }

        public String getDriverLicense() {
                return driverLicense;
        }
        public void setDriverLicense(String driverLicense) {
                this.driverLicense = driverLicense;
        }

        public String getPostalCode() { return postalCode; }
        public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

        public String getEmail() {
                return email;
        }
        public void setEmail(String email) {
                this.email = email;
        }
}
