package Carshop.example.domain;

public class Promotion {

    private int idPromotion;
    private double cashDown;
    private double totalCash;
    private double totalInterset;
    private double allPayment;
    private int monthlyPayment;
    private double downPercent;
    private double interest;
    private int monthlyInstallments;
    private int yearInstallments;


    public double getAllPayment() { return allPayment; }
    public void setAllPayment(double allPayment) { this.allPayment = allPayment; }

    public int getMonthlyPayment() { return monthlyPayment; }
    public void setMonthlyPayment(int monthlyPayment) { this.monthlyPayment = monthlyPayment; }

    public double getCashDown() { return cashDown; }
    public void setCashDown(double cashDown) { this.cashDown = cashDown; }

    public void addPromotion(int idPromotion, double cash){
        if(idPromotion == 1){
            downPercent = 20;
            interest = 0.04;// 4%
            monthlyInstallments = 48;
            yearInstallments = 4;

            cashDown = (cash*downPercent)/100; //หาเงินดาวของราคารถ 20%
            totalCash = cash-cashDown;// ยอดที่ต้องชำระ ลบ เงินดาวแล้ว
            totalInterset = (totalCash*interest)* yearInstallments;//นำมาคูณดอกเบี้ยตลอดระยะเวลาชำระ

            allPayment = (totalCash+totalInterset);//ยอดทั้งหมดที่ต้องชำระ
            monthlyPayment = (int) (allPayment/ monthlyInstallments);//ยอดชำระต่อเดือน
        }else if(idPromotion == 2){
            downPercent = 20;
            interest = 0.085;// 8.5%
            monthlyInstallments = 60;
            yearInstallments = 5;
            cashDown = (cash*downPercent)/100;
            totalCash = cash-cashDown;
            totalInterset = (totalCash*interest)* yearInstallments;
            allPayment = (totalCash+totalInterset);
            monthlyPayment = (int) (allPayment/ monthlyInstallments);
        }else if(idPromotion == 3){
            downPercent = 20;
            interest = 0.1;//1 %
            monthlyInstallments = 72;
            yearInstallments = 6;
            cashDown = (cash*downPercent)/100;
            totalCash = cash-cashDown;
            totalInterset = (totalCash*interest)* yearInstallments;
            allPayment = (totalCash+totalInterset);
            monthlyPayment = (int) (allPayment/ monthlyInstallments);
        }
    }
}
