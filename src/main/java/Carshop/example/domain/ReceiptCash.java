package Carshop.example.domain;

import Carshop.example.Enums.CarShowRoom;
import Carshop.example.Enums.ChooseCar;
import Carshop.example.Enums.ColorCar;

public class ReceiptCash {
    private ChooseCar carName;
    private ColorCar colorCar;
    private CarShowRoom carShowRoom;
    private String licenseCar;
    private String firstName;
    private String lastName;
    private String tel;
    private String address;
    private String postalCode;
    private String email;


    public ChooseCar getCarName() {
        return carName;
    }

    public void setCarName(ChooseCar carName) {
        this.carName = carName;
    }

    public ColorCar getColorCar() {
        return colorCar;
    }

    public void setColorCar(ColorCar colorCar) {
        this.colorCar = colorCar;
    }

    public CarShowRoom getCarShowRoom() {
        return carShowRoom;
    }

    public void setCarShowRoom(CarShowRoom carShowRoom) {
        this.carShowRoom = carShowRoom;
    }

    public String getLicenseCar() {
        return licenseCar;
    }

    public void setLicenseCar(String licenseCar) {
        this.licenseCar = licenseCar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
