package Carshop.example.domain;

import Carshop.example.Enums.ColorCar;
import Carshop.example.Enums.ChooseCar;

public class SearchCar {



    private double higherPrice;
    private double lowerPrice;
    private ColorCar colorCar;
    private ChooseCar chooseCar;


    public double getHigherPrice() { return higherPrice; }
    public void setHigherPrice(double higherPrice) { this.higherPrice = higherPrice; }

    public double getLowerPrice() { return lowerPrice; }
    public void setLowerPrice(double lowerPrice) { this.lowerPrice = lowerPrice; }

    public ColorCar getColorCar() { return colorCar; }
    public void setColorCar(ColorCar colorCar) { this.colorCar = colorCar; }

    public ChooseCar getChooseCar() { return chooseCar; }
    public void setChooseCar(ChooseCar chooseCar) { this.chooseCar = chooseCar; }
}
