package Carshop.example.es;

import Carshop.example.Enums.ColorCar;
import Carshop.example.Enums.ChooseCar;
import Carshop.example.Enums.StatusCar;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "datacar-index")
public class CarEntity {

    @Id
    private String id;
    @Field(type = FieldType.Keyword)
    private ChooseCar carName;
    private ColorCar colorCar;
    @Field(type = FieldType.Double)
    private double price;
    @Field(type = FieldType.Text)
    private String licenseCar;
    @Field(type = FieldType.Keyword)
    private StatusCar statusCar;


    public void setId(String id) { this.id = id; }
    public String getId() { return id; }

    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }

    public String getLicenseCar() { return licenseCar; }
    public void setLicenseCar(String licenseCar) { this.licenseCar = licenseCar; }

    public ColorCar getColorCar() { return colorCar; }
    public void setColorCar(ColorCar colorCar) { this.colorCar = colorCar; }

    public ChooseCar getCarName() { return carName; }
    public void setCarName(ChooseCar carName) { this.carName = carName; }

    public StatusCar getStatusCar() { return statusCar; }
    public void setStatusCar(StatusCar statusCar) { this.statusCar = statusCar; }
}
