package Carshop.example.es;

import Carshop.example.Enums.Gender;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "customer-index")
public class CustomerEntity {


    @Id
    private String idCustomer;
    @Field(type = FieldType.Keyword)
    private String firstName;
    private String lastName;
    private Gender gender;
    @Field(type = FieldType.Integer)
    private int age;
    @Field(type = FieldType.Text)
    private String driverLicense;
    private String address;
    private String postalCode;
    private String email;
    private String tel;

    public String getIdcustomer() { return idCustomer; }
    public void setIdcustomer(String idcustomer) { this.idCustomer = idcustomer; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getTel() { return tel; }
    public void setTel(String tel) { this.tel = tel; }

    public String getDriverLicense() { return driverLicense; }
    public void setDriverLicense(String driverLicense) { this.driverLicense = driverLicense; }

    public String getAddress() { return address; }
    public void setAddress(String address) { this.address = address; }

    public Gender getGender() { return gender; }
    public void setGender(Gender gender) { this.gender = gender; }

    public String getPostalCode() { return postalCode; }
    public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }
}
