package Carshop.example.es;

import Carshop.example.Enums.CarShowRoom;
import Carshop.example.Enums.ChooseCar;
import Carshop.example.Enums.ColorCar;
import Carshop.example.Enums.StatusOrder;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDate;

@Document(indexName = "order-index")
public class OrderEntity {

    @Id
    private String idOrder;
    private String idCar;
    @Field(type = FieldType.Keyword)
    private ChooseCar carName;
    private ColorCar colorCar;
    private StatusOrder statusOrder;
    @Field(type = FieldType.Double)
    private double cashDown;
    private double allPayment;
    private int monthlyPayment;
    @Field(type = FieldType.Text)
    private CarShowRoom carShowRoom;
    private String licenseCar;
    private String firstName;
    private String lastName;
    private String tel;
    private String address;
    private String postalCode;
    private String email;
    @Field(type = FieldType.Date,format = DateFormat.year_month_day)
    private LocalDate date;


    public String getIdOrder() {
        return idOrder;
    }
    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getIdCar() {
        return idCar;
    }
    public void setIdCar(String idCar) {
        this.idCar = idCar;
    }

    public double getAllPayment() {
        return allPayment;
    }
    public void setAllPayment(double allPayment) {
        this.allPayment = allPayment;
    }

    public String getLicenseCar() {
        return licenseCar;
    }
    public void setLicenseCar(String licenseCar) {
        this.licenseCar = licenseCar;
    }

    public ColorCar getColorCar() {
        return colorCar;
    }
    public void setColorCar(ColorCar colorCar) {
        this.colorCar = colorCar;
    }

    public ChooseCar getCarName() {
        return carName;
    }
    public void setCarName(ChooseCar carName) {
        this.carName = carName;
    }

    public CarShowRoom getCarShowRoom() { return carShowRoom; }
    public void setCarShowRoom(CarShowRoom carShowRoom) { this.carShowRoom = carShowRoom; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public StatusOrder getStatusOrder() {
        return statusOrder;
    }
    public void setStatusOrder(StatusOrder statusOrder) {
        this.statusOrder = statusOrder;
    }

    public String getPostalCode() {
        return postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public int getMonthlyPayment() {
        return monthlyPayment;
    }
    public void setMonthlyPayment(int monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public double getCashDown() { return cashDown; }
    public void setCashDown(double cashDown) { this.cashDown = cashDown; }

    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }

}
