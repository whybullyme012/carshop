package Carshop.example.repo;

import Carshop.example.Enums.ChooseCar;
import Carshop.example.es.CarEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface CarRepo extends ElasticsearchRepository<CarEntity, String> {
    List<CarEntity> findAll();
    CarEntity findByCarName(ChooseCar carName);

}
