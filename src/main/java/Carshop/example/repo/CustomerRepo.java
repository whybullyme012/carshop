package Carshop.example.repo;

import Carshop.example.es.CustomerEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface CustomerRepo extends ElasticsearchRepository<CustomerEntity, String> {
    List<CustomerEntity> findAll();

}
