package Carshop.example.repo;

import Carshop.example.es.OrderEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface OrderRepo extends ElasticsearchRepository<OrderEntity, String> {
    List<OrderEntity> findAll();
}
