package Carshop.example.service;

import Carshop.example.Enums.*;
import Carshop.example.domain.Booking;
import Carshop.example.domain.Customer;
import Carshop.example.domain.Promotion;
import Carshop.example.domain.SearchCar;
import Carshop.example.es.CarEntity;
import Carshop.example.es.CustomerEntity;
import Carshop.example.es.OrderEntity;
import Carshop.example.repo.CarRepo;
import Carshop.example.repo.CustomerRepo;
import Carshop.example.repo.OrderRepo;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHitsIterator;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarRepo repo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private ElasticsearchRestTemplate es;

    LocalDate timeNow = LocalDate.now();


    private String generateidcar() {
        return "S" +  String.format("%04d", repo.count());
    }

    private String generateidOrder() {
        return "OR" + String.format("%04d", orderRepo.count());
    }

    public String generateidCustomer(){ return 'C'+String.format("%04d", customerRepo.count()); }

    public String generateLicense(){ return "AB-"+String.format("%03d", repo.count())+"-ZK"; }


    public List<CarEntity> searchDataCar(SearchCar searchCar) {
        List<CarEntity> entities = new ArrayList<>();
        BoolQueryBuilder qb = QueryBuilders.boolQuery();
        if (searchCar.getChooseCar() != null) {
            qb.must(QueryBuilders.matchQuery("carName", searchCar.getChooseCar()));
        }
        if (searchCar.getColorCar() != null) {
            qb.must(QueryBuilders.matchQuery("colorCar", searchCar.getColorCar()));
        }
        if (searchCar.getHigherPrice()>0) {
            qb.must(QueryBuilders.rangeQuery("price").gte(searchCar.getHigherPrice()));
        }
        if(searchCar.getLowerPrice()>0){
            qb.must(QueryBuilders.rangeQuery("price").lte(searchCar.getLowerPrice()));
        }
        if (searchCar.getHigherPrice()>0 && searchCar.getLowerPrice() >0){
            throw new IllegalArgumentException("Please fill in the information only one tpye");
        }
        if (searchCar.getHigherPrice() <0 || searchCar.getLowerPrice() <0){
            throw new NegativeArraySizeException("Cannot enter negative numbers.");
        }

        NativeSearchQueryBuilder search = new NativeSearchQueryBuilder().withQuery(qb);
        SearchHitsIterator<CarEntity> hits = es.searchForStream(search.build(), CarEntity.class);
        hits.forEachRemaining(hit -> entities.add(hit.getContent()));
        return entities;
    }

    public void addCar(ChooseCar chooseCar, ColorCar colorcar) {
        CarEntity entity = new CarEntity();
        if (chooseCar == ChooseCar.MINI_5_DOOR) {
            entity.setPrice(400000);
        } else if (chooseCar == ChooseCar.MINI_CLUBMAN) {
            entity.setPrice(500000);
        } else if (chooseCar == ChooseCar.TOYOTA_VIOS) {
            entity.setPrice(600000);
        } else {
            entity.setPrice(700000);
        }
        entity.setId(generateidcar());
        entity.setCarName(chooseCar);
        entity.setLicenseCar(generateLicense());
        entity.setStatusCar(StatusCar.In_Stock);
        entity.setColorCar(colorcar);
        repo.save(entity);
    }

    public CarEntity getCarById(String id) { return repo.findById(id).get(); }

    public CarEntity getCarByNameCar(ChooseCar carName){return repo.findByCarName(carName);}

    public List<CarEntity> getAlldataCar() {
        return repo.findAll();
    }

    public Booking bookingCar(ChooseCar chooseCar, ColorCar selectColorCar, CarShowRoom showRoom, double cashPledge, ChoosePromotion promotion, Customer customer, Gender gender) {

        CarEntity entity = getCarByNameCar(chooseCar);
        Booking book = new Booking();
        if(cashPledge != 5000){
            throw new IllegalArgumentException("Please Enter Money = 5000");
        }else if(entity.getStatusCar() == StatusCar.Booking){
            throw new IllegalArgumentException("This Car is Out of stock");
        }
            // update Statuscar
            entity.setStatusCar(StatusCar.Booking);
            repo.save(entity);

            // add order
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setDate(timeNow);
            orderEntity.setIdOrder(generateidOrder());
            orderEntity.setIdCar(entity.getId());
            orderEntity.setCarName(chooseCar);
            orderEntity.setColorCar(selectColorCar);
            orderEntity.setStatusOrder(StatusOrder.Waiting_for_payment);
            orderEntity.setCarShowRoom(showRoom);
            orderEntity.setLicenseCar(entity.getLicenseCar());
            orderEntity.setFirstName(customer.getFirstName());
            orderEntity.setLastName(customer.getLastName());
            orderEntity.setTel(customer.getTel());
            orderEntity.setAddress(customer.getAddress());
            orderEntity.setPostalCode(customer.getPostalCode());
            orderEntity.setEmail(customer.getEmail());

            CustomerEntity customerEntity = new CustomerEntity();
            customerEntity.setIdcustomer(generateidCustomer());
            customerEntity.setFirstName(customer.getFirstName());
            customerEntity.setLastName(customer.getLastName());
            customerEntity.setDriverLicense(customer.getDriverLicense());
            customerEntity.setAddress(customer.getAddress());
            customerEntity.setAge(customer.getAge());
            customerEntity.setGender(gender);
            customerEntity.setTel(customer.getTel());

            // Promotion
            Promotion promo = new Promotion();
            if (promotion == ChoosePromotion.Installments_48_Months) {
                promo.addPromotion(1, entity.getPrice());
                orderEntity.setCashDown(promo.getCashDown());
                orderEntity.setAllPayment(promo.getAllPayment());
                orderEntity.setMonthlyPayment(promo.getMonthlyPayment());

            } else if (promotion == ChoosePromotion.Installments_60_Months) {
                promo.addPromotion(2, entity.getPrice());
                orderEntity.setCashDown(promo.getCashDown());
                orderEntity.setAllPayment(promo.getAllPayment());
                orderEntity.setMonthlyPayment(promo.getMonthlyPayment());

            } else if (promotion == ChoosePromotion.Installments_72_Months) {
                promo.addPromotion(3, entity.getPrice());
                orderEntity.setCashDown(promo.getCashDown());
                orderEntity.setAllPayment(promo.getAllPayment());
                orderEntity.setMonthlyPayment(promo.getMonthlyPayment());

            } else {
                orderEntity.setAllPayment(entity.getPrice());
            }
            // Return Order
            book.setBookingDate(orderEntity.getDate());
            book.setIdOrder(orderEntity.getIdOrder());
            book.setCarName(orderEntity.getCarName());
            book.setColorCar(orderEntity.getColorCar());
            book.setCarShowRoom(orderEntity.getCarShowRoom());
            book.setFirstName(customer.getFirstName());
            book.setLastName(customer.getLastName());
            book.setTel(customer.getTel());
            book.setAddress(customer.getAddress());
            book.setPostalCode(customer.getPostalCode());
            book.setEmail(customer.getEmail());
            book.setCashDown(orderEntity.getCashDown());
            book.setAllPayment(orderEntity.getAllPayment());
            book.setMonthlyPayment(orderEntity.getMonthlyPayment());

            orderRepo.save(orderEntity);
            customerRepo.save(customerEntity);


        return book;
    }
}

