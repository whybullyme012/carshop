package Carshop.example.service;

import Carshop.example.Enums.Gender;
import Carshop.example.domain.Customer;
import Carshop.example.es.CustomerEntity;
import Carshop.example.repo.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private CarService carService;



    public List<CustomerEntity> getAllDataCustomer() { return customerRepo.findAll(); }

    public CustomerEntity addCustomer(Customer customer, Gender gender) {
        CustomerEntity entity = new CustomerEntity();
        entity.setIdcustomer(carService.generateidCustomer());
        entity.setFirstName(customer.getFirstName());
        entity.setLastName(customer.getLastName());
        entity.setGender(gender);
        entity.setAge(customer.getAge());
        entity.setTel(customer.getTel());
        entity.setDriverLicense(customer.getDriverLicense());
        entity.setAddress(customer.getAddress());
        entity.setPostalCode(customer.getPostalCode());
        entity.setEmail(customer.getEmail());
        return customerRepo.save(entity);}
    }

