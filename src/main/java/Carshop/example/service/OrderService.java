package Carshop.example.service;

import Carshop.example.Enums.StatusCar;
import Carshop.example.Enums.StatusOrder;
import Carshop.example.domain.ReceiptCash;
import Carshop.example.domain.ReceiptMonth;
import Carshop.example.es.CarEntity;
import Carshop.example.es.OrderEntity;
import Carshop.example.repo.CarRepo;
import Carshop.example.repo.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {


    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private CarRepo car;


    public List<OrderEntity> getAllOrder() { return orderRepo.findAll(); }
    public OrderEntity getIdorderById(String id) { return orderRepo.findById(id).get(); }

    public ReceiptCash cashPayment(String order, double cash) {

        ReceiptCash receiptCash = new ReceiptCash();
        OrderEntity orderEntity = getIdorderById(order);
        CarEntity carEntity = new CarEntity();
        if(orderEntity.getAllPayment()< cash){
            throw new IllegalArgumentException("Please Pay in full");
        }else if(orderEntity.getStatusOrder()==StatusOrder.Paid) {
            throw new IllegalArgumentException("NumberOrder Status = paid \n Please Try again");
        }else{
            orderEntity.setStatusOrder(StatusOrder.Paid);
            orderEntity.setAllPayment(0);
            car.findById(orderEntity.getIdCar());
            carEntity.setStatusCar(StatusCar.Soldout);

            receiptCash.setAddress(orderEntity.getAddress());
            receiptCash.setCarName(orderEntity.getCarName());
            receiptCash.setEmail(orderEntity.getEmail());
            receiptCash.setColorCar(orderEntity.getColorCar());
            receiptCash.setFirstName(orderEntity.getFirstName());
            receiptCash.setLastName(orderEntity.getLastName());
            receiptCash.setLicenseCar(orderEntity.getLicenseCar());
            receiptCash.setCarShowRoom(orderEntity.getCarShowRoom());
            receiptCash.setTel(orderEntity.getTel());
            receiptCash.setPostalCode(orderEntity.getPostalCode());

            car.save(carEntity);
            orderRepo.save(orderEntity);
        }
        return receiptCash;

    }

    public ReceiptMonth monthPayment(String order, double cash){
        OrderEntity orderEntity = getIdorderById(order);
        ReceiptMonth receipt = new ReceiptMonth();
        CarEntity carEntity = new CarEntity();

        if(orderEntity.getStatusOrder()==StatusOrder.Paid){
            throw new IllegalArgumentException("NumberOrder Status = paid \n Please Try again");
        }if(cash <=0){
            throw new IllegalArgumentException("Please Enter Cash Try again");
        }else if(orderEntity.getAllPayment()>0 && cash %2==0){
            orderEntity.setAllPayment(orderEntity.getAllPayment()-cash);

            if(orderEntity.getAllPayment()<=0){
                orderEntity.setStatusOrder(StatusOrder.Paid);
                orderEntity.setAllPayment(0);
                car.findById(orderEntity.getIdCar());
                carEntity.setStatusCar(StatusCar.Soldout);
                carEntity.setId(orderEntity.getIdCar());
                carEntity.setColorCar(orderEntity.getColorCar());
                carEntity.setLicenseCar(orderEntity.getLicenseCar());
                carEntity.setCarName(orderEntity.getCarName());
                car.save(carEntity);
            }
        }


        receipt.setAddress(orderEntity.getAddress());
        receipt.setEmail(orderEntity.getEmail());
        receipt.setFirstName(orderEntity.getFirstName());
        receipt.setLastName(orderEntity.getLastName());
        receipt.setLicenseCar(orderEntity.getLicenseCar());
        receipt.setTel(orderEntity.getTel());
        receipt.setPostalCode(orderEntity.getPostalCode());
        receipt.setAllPayment(orderEntity.getAllPayment());
        orderRepo.save(orderEntity);


        return receipt;
    }

}

